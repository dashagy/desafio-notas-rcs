class Note {
  constructor(title, content, date, category) {
    this.title = title;
    this.content = content;
    this.generateID();
    this.date = date;
    this.category = category;
  }

  generateID() {
    let id = "";
    let num;
    for (let i = 0; i < 20; i++) {
      num = Math.floor(Math.random() * (122 - 48) + 48);
      if (
        (48 <= num && num <= 57) ||
        (65 <= num && num <= 90) ||
        (97 <= num && num <= 122)
      ) {
        id += String.fromCharCode(num);
      } else i--;
    }
    this.id = id;
  }
}

function dateIssued() {
  let date = new Date();
  date = date.toLocaleString();
  return date;
}

function selectAction() {
  return document.getElementById("selectActionForm").options.selectedIndex;
}

function showSelectedForm() {
  let option = selectAction();
  switch (option) {
    case 0:
      document.getElementById("searchNoteForm").setAttribute("hidden", true);
      document.getElementById("saveNoteForm").removeAttribute("hidden");
      break;

    case 1:
      document.getElementById("saveNoteForm").setAttribute("hidden", true);
      document.getElementById("searchNoteForm").removeAttribute("hidden");
      break;
  }
}

function setCategoryOptionsValues() {
  let categories = document.getElementById("selectNoteCategory").children;
  for (let i = 0; i < categories.length; i++) {
    categories[i].setAttribute("Value", categories[i].value);
  }
}

function setCreateCategoryForm() {
  document.getElementById("saveNoteForm").setAttribute("hidden", true);
  document.getElementById("searchNoteForm").setAttribute("hidden", true);
  document.getElementById("selectActionForm").setAttribute("hidden", true);
  document.getElementById("createCategoryForm").removeAttribute("hidden");
  document.getElementById("newCategoryInput").focus();
}

function saveNewCategory(event) {
  event.preventDefault();
  let newCategoryHTML = document.getElementById("newCategoryInput");
  let newCategory = newCategoryHTML.value;
  if (validateInputLength(newCategory, 30)) {
    if (!dbCategories.includes(newCategory)) {
      dbCategories.push(newCategory);
      localStorage.setItem("dbCategories", JSON.stringify(dbCategories));
      alert(`Categoría "${newCategory}" creada satisfactoriamente`);
      setCategoriesOptions(dbCategories);
      location.reload();
    } else alert("Ya existe una categoría con ese nombre");
    newCategoryHTML.value = "";
  } else alert("El nombre de la categoría no debe ser entre 1 y 30 caracteres");
}

function pressSaveButton(event) {
  if (event.keyCode == 13) {
    event.preventDefault();
    document.getElementById("saveNewCategoryButton").click();
  } else return false;
}

function setCategoriesOptions(categories) {
  let optionsForm = document.getElementById("selectNoteCategory");
  let optionsFormModal = document.getElementsByClassName(
    "selectNoteCategoryModal"
  );
  optionsForm.innerHTML = "";
  let optionsHTML = `<option selected class="text-muted">Seleccione categoría</option>`;
  categories.map(
    (categoria) => (optionsHTML += `<option>${categoria}</option>`)
  );
  optionsForm.innerHTML = optionsHTML;
  for (let i = 0; i < optionsFormModal.length; i++) {
    optionsFormModal[i].innerHTML = optionsHTML;
  }
}

function setModalCategoriesOptions() {
  for (let i = 0; i < dbNotes.length; i++) {
    let id = dbNotes[i].id;
    let optionsFormModal = document
      .getElementById(`editForm-` + id)
      .getElementsByClassName("selectNoteCategoryModal")[0];
    let noteCategory = dbNotes.filter((note) => note.id == id)[0].category;
    let optionIndex = dbCategories.indexOf(noteCategory);
    optionsFormModal.selectedIndex = optionIndex + 1;
  }
}

function deleteCategory() {
  let categoryToDelete = document.getElementById("selectNoteCategory");
  let notesCategoriesArray = dbNotes.map(function (note) {
    return note.category;
  });
  notesCategoriesArray = notesCategoriesArray.filter(
    (categoryNotes) => categoryNotes == categoryToDelete.value
  );
  if (categoryToDelete.options.selectedIndex != 0) {
    if (
      confirm(
        `¿Seguro que quiere eliminar la categoría "${categoryToDelete.value}"?`
      )
    ) {
      if (!notesCategoriesArray.length) {
        let indexToDelete = categoryToDelete.options.selectedIndex;
        dbCategories = dbCategories.filter(
          (categoria) => dbCategories.indexOf(categoria) != indexToDelete - 1
        );
        localStorage.removeItem("dbCategories");
        localStorage.setItem("dbCategories", JSON.stringify(dbCategories));
        alert(
          `Categoría "${categoryToDelete.value}" eliminada satisfactoriamente`
        );
        setCategoriesOptions(dbCategories);
      } else
        alert(
          `No deben existir notas de una categoría para poder eliminarla.
          \nLa categoría "${categoryToDelete.value}" tiene ${notesCategoriesArray.length} nota/s restante/s`
        );
    }
  } else alert("Seleccione una categoría para eliminar");
}

function saveNote(event) {
  event.preventDefault();
  let date = dateIssued();
  let title = document.getElementById("titulo");
  let content = document.getElementById("contenido");
  let select = document.getElementById("selectNoteCategory");
  let category =
    select.options[select.selectedIndex] === undefined
      ? null
      : select.options[select.selectedIndex].value;
  if (
    validateInputLength(title.value, 60) &&
    validateInputLength(content.value, 500)
  ) {
    if (category && select.options.selectedIndex != 0) {
      let note = new Note(title.value, content.value, date, category);
      dbNotes.push(note);
      localStorage.setItem("dbNotes", JSON.stringify(dbNotes));
      UPDATE();
      title.value = "";
      content.value = "";
      document.getElementById("title-span").innerHTML = "0/60";
      document.getElementById("content-span").innerHTML = "0/500";
      select.options.selectedIndex = "0";
      title.focus();
    } else alert("Debe seleccionar una categoría válida");
  } else
    alert("Ingrese un titulo de hasta 60 caracteres y contenido de hasta 500");
}

function showNotes(notes) {
  let noteColumn = document.getElementById("noteContainer");
  noteColumn.innerHTML = "";
  let cardNote = "";
  notes.map((nota) => {
    cardNote +=
      `<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 d-flex flex-column">` +
      cardHTML(nota.title, nota.content, nota.id, nota.date, nota.category) +
      modalHTML(nota.title, nota.content, nota.id) +
      `</div>`;
  });
  noteColumn.innerHTML = cardNote;
}

function editNote(event, id) {
  event.preventDefault();
  let title = document.getElementById(`titulo-` + id);
  let content = document.getElementById(`contenido-` + id);
  let option = document.getElementById(`categoria-` + id).options;
  let category = option[option.selectedIndex].value;
  let indice = dbNotes.findIndex((nota) => nota.id === id);
  if (
    validateInputLength(title.value, 60) &&
    validateInputLength(content.value, 500)
  ) {
    if (option.selectedIndex != 0) {
      dbNotes[indice].title = title.value;
      dbNotes[indice].content = content.value;
      dbNotes[indice].date = dateIssued();
      dbNotes[indice].category = category;
      localStorage.removeItem("dbNotes");
      localStorage.setItem("dbNotes", JSON.stringify(dbNotes));
      UPDATE();
      form = document.getElementById(`editForm-` + id).submit();
    } else alert("Debe seleccionar una categoría válida");
  } else
    alert(
      "Fallo en la modificación de la nota. Ingrese un titulo de hasta 60 caracteres y contenido de hasta 500"
    );
}

function deleteNote(id) {
  if (id && confirm("¿Seguro que quiere eliminar esta nota?")) {
    dbNotes = dbNotes.filter((nota) => nota.id !== id);
    localStorage.removeItem("dbNotes");
    localStorage.setItem("dbNotes", JSON.stringify(dbNotes));
    showNotes(dbNotes);
  }
}

function searchNote(event) {
  event.preventDefault();
  let searchInput = document.getElementById("searchInput");
  let regex = new RegExp(searchInput.value, "ig");
  let searchArray = [];
  let searchArrayIndex = 0;
  for (let i = 0; i < dbNotes.length; i++) {
    if (
      regex.test(dbNotes[i].title) ||
      regex.test(dbNotes[i].content) ||
      regex.test(dbNotes[i].category)
    ) {
      searchArray[searchArrayIndex] = dbNotes[i];
      searchArrayIndex++;
    }
  }
  showNotes(searchArray);
  searchInput.value = "";
}

function inputCharCounter(event, id, maxLenght) {
  event.preventDefault();
  let inputField = document.getElementById(id);
  let stringLenght = String(inputField.value).length;
  let span = inputField.nextElementSibling;
  span.innerHTML = stringLenght + "/" + maxLenght;
  if (stringLenght > maxLenght) {
    span.classList.add("text-danger", "text-bold");
    span.classList.remove("text-muted");
  } else {
    span.classList.remove("text-danger", "text-bold");
    span.classList.add("text-muted");
  }
}

function validateInputLength(string, max_lenght) {
  if (0 < string.length && string.length <= max_lenght) return true;
  else return false;
}

function cardHTML(title, content, id, date, category) {
  return `<div class="card my-2 flex-grow-1">
            <div class="card-body d-flex flex-column p-0">
              <div class="card-header text-muted font-italic pl-1 p-0 d-flex justify-content-between">
                <div class="align-self-end"><p class="m-0">${category}</p></div>
                <div class="btn-group" role="group">
                  <button class="btn btn-sm btn-outline-info" data-toggle="modal" data-target="#note-${id}" style="border: none;"><i class="fas fa-edit"></i></button>
                  <button class="btn btn-sm btn-outline-danger" onclick="deleteNote('${id}')" style="padding-left:11px; padding-right:11px; border: none;"><i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-title px-2 my-1">
                <h5 class="p-0">${title}</h5>
              </div>
              <div class="card-text pl-3 pr-1 pb-2">
                ${content}
              </div>
              <div class="card-footer text-muted font-italic align-left mt-auto p-1">
                <small>Ultima modificación:   ${date}</small>
              </div>
            </div>
          </div>`;
}

function modalHTML(title, content, id) {
  return `<div class="modal fade" id="note-${id}" tabindex="-1" role="dialog" aria-labelledby="'${title}'" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header bg-secondary">
                  <h5 class="modal-title text-light">Editar nota</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <i class="fas fa-times"></i>
                  </button>
                </div>
                <div class="modal-body">
                  <form id="editForm-${id}">
                    <input
                      type="text"
                      class="form-control shadow mb-1"
                      name="titulo"
                      id="titulo-${id}"
                      placeholder="Título"
                      value="${title}"
                      onclick="this.select()"
                    />
                    <textarea
                      class="form-control shadow mb-1"
                      name="contenido"
                      id="contenido-${id}"
                      placeholder="Contenido"
                      onclick="this.select()"
                    >${content}</textarea>
                    <div class="form-group shadow mb-1">
                      <select class="form-control selectNoteCategoryModal" id="categoria-${id}"> </select>
                    </div>
                  </form>
                </div>
                <div class="modal-footer bg-secondary">
                  <button type="button" form="editForm-${id}" class="btn btn-dark" onclick="editNote(event,'${id}')"><i class="fas fa-plus p-1"></i>Guardar Cambios</button>
                </div>
              </div>
            </div>
          </div>`;
}

function UPDATE() {
  showNotes(dbNotes);
  setCategoryOptionsValues();
  setCategoriesOptions(dbCategories);
  setModalCategoriesOptions();
}

let dbNotes = JSON.parse(localStorage.getItem("dbNotes")) || [];
let dbCategories = JSON.parse(localStorage.getItem("dbCategories")) || [];
UPDATE();
